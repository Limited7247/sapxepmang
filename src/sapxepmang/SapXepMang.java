/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sapxepmang;

import java.util.Scanner;

/**
 *
 * @author Limited
 */
class Integer
{
    private int x;
    public Integer() {x = 0;}
    public Integer(int xIn) {x = xIn;}
    public Integer(Integer xIn) {x = xIn.x;}
    public int Get() {return x;}
    public void Set(int xIn) {x = xIn;}
    public void Set(Integer xIn) {x = xIn.x;}
    
    public void Swap(Integer a, Integer b)
    {
        Integer temp = new Integer(a);
        a.Set(b);
        b.Set(temp);
    }
}

public class SapXepMang {

    /**
     * @param args the command line arguments
     */
    
    
    static int n;
    static Integer[] A; ///= new Integer[1000];
    static Scanner jin = new Scanner(System.in);
    
    static void InputArray()
    {
        
        System.out.print("Enter n = "); n = jin.nextInt();
        A = new Integer[n]; int x;
        for (int i= 0; i<n; i++)
        {
            System.out.print("Enter A[" + i + "] = ");
            x= jin.nextInt();
            A[i] = new Integer(x);
        }
        System.out.println();
    }
    
    static void ShowArray()
    {
        System.out.print("Your Array: ");
        for (int i= 0; i<n; i++)
            System.out.print(A[i].Get() + " ");
        System.out.println();
    }
    
    static void Swap(Integer a, Integer b)
    {
        Integer k = new Integer(a);   /// k = a;
        a.Set(b); /// a = b;
        b.Set(k);       /// b= k;
    }
    
    /// Sap xep Noi bot
    static void BubbleSort()
    {
        int k;
        for (int i= 0; i<n-1; i++)
            for (int j= i+1; j<n; j++)
                if (A[i].Get() < A[j].Get())
                {
                    /*
                    k = A[i];
                    A[i]= A[j];
                    A[j]= k;
                    */
                    Swap(A[i], A[j]);                  
                }
    }
    
    /// Not done here! - Impossible to Insert first
    static void InsertionSort()
    {
        int j;
        for(int i= 1; i<n; i++)
        {
           j= i-1;
           while ((j >= 0) && (A[j].Get() > A[i].Get()))
               j--;
           
           if (i != j) 
           {
               Swap(A[i], A[j]);
           }
        }
    }
    
    /// Sap xep tron
    static void merge(int first, int middle, int last) {
        int[] temp = new int[last+1];
        int first1, last1, first2, last2;
        int index = first;

        first1 = first;
        last1 = middle;
        first2 = middle+1;
        last2 = last;

        while((first1 <= last1) && (first2 <= last2)) {
            if(A[first1].Get() < A[first2].Get()) {
                temp[index] = A[first1].Get();
                index++;
                first1++;                  
            }
            else {
                temp[index] = A[first2].Get();
                index++;
                first2++;
            }
        }

        if(first2 > last2) {
        while(first1 <= last1) {
            temp[index] = A[first1].Get();
            index++;
            first1++;
            }
        }

        if(first1 > last1) {
            while(first2 <= last2) {
                 temp[index] = A[first2].Get();
                 index++;
                 first2++;
            }
        }           

        for(index = first; index <= last; index ++) {
            A[index].Set(temp[index]);
        }
        
    }

    static void mergeSort(int first, int last) {
        if (first < last) {
            int middle = (first + last) / 2;
            mergeSort(first, middle);
            mergeSort(middle + 1, last);
            merge(first, middle, last);
        }
    }

    /// Sap xep chon
    static void SelectionSort()
    {
        int iMin;
        for (int i = 0; i<n-1; i++)
        {
            iMin= i;
            for (int j = i+1; j<n; j++)
            {
                if (A[j].Get() < A[iMin].Get()) iMin = j;
            }
            Swap(A[i], A[iMin]);
        }
    }
    
    ///Sap xep vun dong
    static void heapSort(int count) 
    {
        int end = count; 

        MakeHeap(count); 
        while (end > 0)     
        {
            Swap(A[end], A[1]);
            end--;
            DownHeap(0, end);
        }
    }

    static void MakeHeap(int count) 
    {
        int start = count / 2;

        while (start > 0)
        {
            DownHeap(start, count);
            start--;
        }
     }

    static void DownHeap(int start, int count) 
    {
        int i = start, j;
        j = i * 2;
        while (j <= count) 
        { 
            if ((j+1 <= count) && (A[j].Get() < A[j + 1].Get()))
                j++;
            if (A[i].Get() < A[j].Get())
            {
                Swap(A[i], A[j]);
                i= j;
                j*= 2;
            }
            else
                return;
        }
    }
    
    static void quicksort(int lo, int hi)
    {
        if (lo < hi)
        {
            int p = partition(lo, hi);
            quicksort(lo, p - 1);
            quicksort(p + 1, hi);
        }
    }
    
    static int partition(int lo, int hi)
    {
        int pivot = A[hi].Get();
        int i = lo; ///place for swapping
        for (int j = lo; j<hi; j++)
            if (A[j].Get() <= pivot)
            {
                Swap(A[i], A[j]);
                i++;
            }
        Swap(A[i], A[hi]);
        return i;
    }
            
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("Hello Friend!");
        System.out.println("Sorting Array Program.");
        System.out.println();
       
        InputArray();
        
        ///System.out.println("Bubble Sort: ");
        ///BubbleSort();
        ShowArray();
        
        ///System.out.println("Insertion Sort: ");
        ///InsertionSort();
        ///ShowArray();
        
        ///System.out.println("Selection Sort: ");
        ///SelectionSort();
        ///ShowArray();

        ///System.out.println("Merge Sort: ");
        ///mergeSort(0, n-1);
        ///ShowArray();
        
        ///System.out.println("Heap Sort: ");
        ///heapSort(n);
        ///ShowArray();
        
        System.out.println("Quick Sort: ");
        quicksort(0, n-1);
        ShowArray();
    }
    
}
